
type EffectFn1<A,B> = (a: A) =>  B;
type EffectFn2<A,B,C> = (a: A, b: B) => C;
type EffectFn3<A,B,C,D> = (a: A, b: B, c: C) => D;
type EffectFn4<A,B,C,D,E> = (a: A, b: B, c: C, d: D) => E;
type EffectFn5<A,B,C,D,E,F> = (a: A, b: B, c: C, d: D, e: E) => F;


type Effect<A> = () => A;

type Unit = void;

type PSValue = unknown;
