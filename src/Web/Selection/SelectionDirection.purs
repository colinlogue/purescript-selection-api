module Web.Selection.SelectionDirection where

import Data.Maybe (Maybe(..))


data SelectionDirection
  = Forward
  | Backward
  | Left
  | Right

toString :: SelectionDirection -> String
toString Forward = "forward"
toString Backward = "backward"
toString Left = "left"
toString Right = "right"

fromString :: String -> Maybe SelectionDirection
fromString "forward" = Just Forward
fromString "backward" = Just Backward
fromString "left" = Just Left
fromString "right" = Just Right
fromString _ = Nothing
