module Web.Selection.Range
  ( Range
  , collapse
  , createRange
  , selectNodeContents
  ) where

import Prelude

import Effect (Effect)
import Effect.Uncurried (EffectFn1, EffectFn2, runEffectFn1, runEffectFn2)
import Web.DOM (Document, Node)


foreign import data Range :: Type

foreign import _createRange :: EffectFn1 Document Range

createRange :: Document -> Effect Range
createRange = runEffectFn1 _createRange

foreign import _selectNodeContents :: EffectFn2 Node Range Unit

selectNodeContents :: Node -> Range -> Effect Unit
selectNodeContents = runEffectFn2 _selectNodeContents

foreign import _collapse :: EffectFn2 Boolean Range Unit

collapse :: Boolean -> Range -> Effect Unit
collapse = runEffectFn2 _collapse
