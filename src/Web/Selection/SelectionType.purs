module Web.Selection.SelectionType where

import Data.Maybe (Maybe(..))


data SelectionType
  = None
  | Caret
  | Range

toString :: SelectionType -> String
toString None = "None"
toString Caret = "Caret"
toString Range = "Range"

fromString :: String -> Maybe SelectionType
fromString "None" = Just None
fromString "Caret" = Just Caret
fromString "Range" = Just Range
fromString _ = Nothing
