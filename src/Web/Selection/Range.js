export const _createRange = doc => doc.createRange();
export const _selectNodeContents = (node, range) => range.selectNodeContents(node);
export const _collapse = (toStart, range) => range.collapse(toStart);
