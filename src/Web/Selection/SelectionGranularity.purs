module Web.Selection.SelectionGranularity where

import Data.Maybe (Maybe(..))


data SelectionGranularity
  = Character
  | Word
  | Sentence
  | Line
  | Paragraph
  | LineBoundary
  | SentenceBoundary
  | ParagraphBoundary
  | DocumentBoundary

toString :: SelectionGranularity -> String
toString Character = "character"
toString Word = "word"
toString Sentence = "sentence"
toString Line = "line"
toString Paragraph = "paragraph"
toString LineBoundary = "lineboundary"
toString SentenceBoundary = "sentenceboundary"
toString ParagraphBoundary = "paragraphboundary"
toString DocumentBoundary = "documentboundary"

fromString :: String -> Maybe SelectionGranularity
fromString "character" = Just Character
fromString "word" = Just Word
fromString "sentence" = Just Sentence
fromString "line" = Just Line
fromString "paragraph" = Just Paragraph
fromString "lineboundary" = Just LineBoundary
fromString "sentenceboundary" = Just SentenceBoundary
fromString "paragraphboundary" = Just ParagraphBoundary
fromString "documentboundary" = Just DocumentBoundary
fromString _ = Nothing
