

export const _createRange: EffectFn1<Document, Range> =
  doc => doc.createRange();

export const _selectNodeContents: EffectFn2<Node, Range, Unit> =
  (node, range) => range.selectNodeContents(node);

export const _collapse: EffectFn2<boolean, Range, Unit> =
  (toStart, range) => range.collapse(toStart);
