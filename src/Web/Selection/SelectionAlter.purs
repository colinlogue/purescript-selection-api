module Web.Selection.SelectionAlter where

import Data.Maybe (Maybe(..))


data SelectionAlter
  = Move
  | Extend

toString :: SelectionAlter -> String
toString Move = "move"
toString Extend = "extend"

fromString :: String -> Maybe SelectionAlter
fromString "move" = Just Move
fromString "extend" = Just Extend
fromString _ = Nothing
