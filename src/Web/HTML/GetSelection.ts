export const _getSelection: EffectFn3<(val: PSValue) => PSValue, PSValue, Window | Document, PSValue> =
  (just, nothing, w) => {
    const s = w.getSelection();
    return s ? just(s) : nothing;
  };
