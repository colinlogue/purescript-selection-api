export const _getSelection = (just, nothing, w) => {
    const s = w.getSelection();
    return s ? just(s) : nothing;
};
