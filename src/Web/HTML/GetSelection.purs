module Web.HTML.GetSelection where

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Uncurried (EffectFn3, runEffectFn3)
import Web.DOM (Document)
import Web.HTML.Window (Window)
import Web.Selection (Selection)


class GetSelection a where
  getSelection :: a -> Effect (Maybe Selection)

instance GetSelection Window where
  getSelection = unsafeGetSelection

instance GetSelection Document where
  getSelection = unsafeGetSelection

-- Since we can't do constraints
foreign import _getSelection :: forall a b. EffectFn3 (a -> Maybe a) (Maybe a) b (Maybe Selection)

unsafeGetSelection :: forall a. a -> Effect (Maybe Selection)
unsafeGetSelection = runEffectFn3 _getSelection Just Nothing
