export const _anchorNode: EffectFn3<(val: PSValue) => PSValue, PSValue, Selection, PSValue> =
  (just, nothing, s) => {
    const n = s.anchorNode;
    return n ? just(n) : nothing;
  };

export const _anchorOffset: EffectFn1<Selection, number> =
  s => s.anchorOffset;

export const _focusNode: EffectFn3<(val: PSValue) => PSValue, PSValue, Selection, PSValue> =
  (just, nothing, s) => {
    const n = s.focusNode;
    return n ? just(n) : nothing;
  };

export const _focusOffset: EffectFn1<Selection, Unit> =
  s => s.focusOffset;

export const _isCollapsed: EffectFn1<Selection, Boolean> =
  s => s.isCollapsed;

export const _rangeCount: EffectFn1<Selection, number> =
  s => s.rangeCount;

export const _type: EffectFn4<PSValue, PSValue, PSValue, Selection, PSValue> =
  (none, caret, range, s) => {
    const { type } = s;
    switch (type) {
      case 'None': return none;
      case 'Caret': return caret;
      case 'Range': return range;
      default:
        throw new Error('unknown selection type ' + type);
    }
  };

export const _addRange: EffectFn2<Range, Selection, Unit> =
  (r, s) => s.addRange(r);

export const _collapse: EffectFn3<Node, number, Selection, Unit> =
  (node, offset, s) => s.collapse(node, offset);

export const _collapseToEnd: EffectFn1<Selection, Unit> =
  s => s.collapseToEnd();

export const _collapseToStart: EffectFn1<Selection, Unit> =
  s => s.collapseToStart();

export const _containsNode: EffectFn3<Node, boolean, Selection, boolean> =
  (node, partialContainment, s) => s.containsNode(node, partialContainment);

export const _deleteFromDocument: EffectFn1<Selection, Unit> =
  s => s.deleteFromDocument();

export const _extend: EffectFn3<Node, number, Selection, Unit> =
  (node, offset, s) => s.extend(node, offset);

export const _getRangeAt: EffectFn2<number, Selection, Unit> =
  (index, s) => s.getRangeAt(index);

export const _modify: EffectFn4<string, string, string, Selection, Unit> =
  (alter, direction, granularity, s) => s.modify(alter, direction, granularity);

export const _removeRange: EffectFn2<Range, Selection, Unit> =
  (range, s) => s.removeRange(range);

export const _removeAllRanges: EffectFn1<Selection, Unit> =
  s => s.removeAllRanges();

export const _selectAllChildren: EffectFn2<Node, Selection, Unit> =
  (parentNode, s) => s.selectAllChildren(parentNode);

export const _setBaseAndExtent: EffectFn5<Node, number, Node, number, Selection, Unit> =
  (anchorNode, anchorOffset, focusNode, focusOffset, s) => s.setBaseAndExtent(anchorNode, anchorOffset, focusNode, focusOffset);

export const _toString: EffectFn1<Selection, string> =
  s => s.toString();
