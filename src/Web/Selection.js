export const _anchorNode = (just, nothing, s) => {
    const n = s.anchorNode;
    return n ? just(n) : nothing;
};
export const _anchorOffset = s => s.anchorOffset;
export const _focusNode = (just, nothing, s) => {
    const n = s.focusNode;
    return n ? just(n) : nothing;
};
export const _focusOffset = s => s.focusOffset;
export const _isCollapsed = s => s.isCollapsed;
export const _rangeCount = s => s.rangeCount;
export const _type = (none, caret, range, s) => {
    const { type } = s;
    switch (type) {
        case 'None': return none;
        case 'Caret': return caret;
        case 'Range': return range;
        default:
            throw new Error('unknown selection type ' + type);
    }
};
export const _addRange = (r, s) => s.addRange(r);
export const _collapse = (node, offset, s) => s.collapse(node, offset);
export const _collapseToEnd = s => s.collapseToEnd();
export const _collapseToStart = s => s.collapseToStart();
export const _containsNode = (node, partialContainment, s) => s.containsNode(node, partialContainment);
export const _deleteFromDocument = s => s.deleteFromDocument();
export const _extend = (node, offset, s) => s.extend(node, offset);
export const _getRangeAt = (index, s) => s.getRangeAt(index);
export const _modify = (alter, direction, granularity, s) => s.modify(alter, direction, granularity);
export const _removeRange = (range, s) => s.removeRange(range);
export const _removeAllRanges = s => s.removeAllRanges();
export const _selectAllChildren = (parentNode, s) => s.selectAllChildren(parentNode);
export const _setBaseAndExtent = (anchorNode, anchorOffset, focusNode, focusOffset, s) => s.setBaseAndExtent(anchorNode, anchorOffset, focusNode, focusOffset);
export const _toString = s => s.toString();
