module Web.Selection
  ( Selection
  , anchorNode
  , anchorOffset
  , focusNode
  , focusOffset
  , isCollapsed
  , rangeCount
  , type_
  , addRange
  , collapse
  , collapseToEnd
  , collapseToStart
  , containsNode
  , deleteFromDocument
  , extend
  , getRangeAt
  , modify
  , removeAllRanges
  , removeRange
  , selectAllChildren
  , setBaseAndExtent
  , toString
  ) where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Uncurried (EffectFn1, EffectFn2, EffectFn3, EffectFn4, EffectFn5, runEffectFn1, runEffectFn2, runEffectFn3, runEffectFn4, runEffectFn5)
import Web.DOM (Node)
import Web.Selection.Range (Range)
import Web.Selection.SelectionType (SelectionType)
import Web.Selection.SelectionType as SelectionType

-- | A `Selection` object represents the range of text selected by the user or
-- | the current position of the caret. To obtain a `Selection` object for
-- | examination or manipulation, call `window.getSelection()`.
-- |
-- | A user may make a selection from left to right (in document order) or right
-- | to left (reverse of document order). The ***anchor*** is where the user
-- | began the selection and the ***focus*** is where the user ends the
-- | selection. If you make a selection with a desktop mouse, the anchor is
-- | placed where you pressed the mouse button, and the focus is placed where
-- | you released the mouse button.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection)
foreign import data Selection :: Type

foreign import _anchorNode :: EffectFn3 (Node -> Maybe Node) (Maybe Node) Selection (Maybe Node)

-- | Returns the `Node` in which the selection begins. Can return `null` if
-- | selection never existed in the document (e.g., an iframe that was never
-- | clicked on).
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.anchornode)
anchorNode :: Selection -> Effect (Maybe Node)
anchorNode = runEffectFn3 _anchorNode Just Nothing

foreign import _anchorOffset :: EffectFn1 Selection Int

-- | Returns a number representing the offset of the selection's anchor within
-- | the `anchorNode`. If `anchorNode` is a text node, this is the number of
-- | characters within anchorNode preceding the anchor. If `anchorNode` is an
-- | element, this is the number of child nodes of the `anchorNode` preceding
-- | the anchor.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.anchoroffset)
anchorOffset :: Selection -> Effect Int
anchorOffset = runEffectFn1 _anchorOffset

foreign import _focusNode :: EffectFn3 (Node -> Maybe Node) (Maybe Node) Selection (Maybe Node)

-- | Returns the `Node` in which the selection ends. Can return `null` if
-- | selection never existed in the document (for example, in an `iframe` that
-- | was never clicked on).
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.focusnode)
focusNode :: Selection -> Effect (Maybe Node)
focusNode = runEffectFn3 _focusNode Just Nothing

foreign import _focusOffset:: EffectFn1 Selection Int

-- | Returns a number representing the offset of the selection's anchor within
-- | the `focusNode`. If `focusNode` is a text node, this is the number of
-- | characters within `focusNode` preceding the focus. If `focusNode` is an
-- | element, this is the number of child nodes of the `focusNode` preceding the
-- | focus.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.focusoffset)
focusOffset :: Selection -> Effect Int
focusOffset = runEffectFn1 _focusOffset

foreign import _isCollapsed :: EffectFn1 Selection Boolean

-- | Returns a Boolean indicating whether the selection's start and end points
-- | are at the same position.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.iscollapsed)
isCollapsed :: Selection -> Effect Boolean
isCollapsed = runEffectFn1 _isCollapsed

foreign import _rangeCount :: EffectFn1 Selection Int

-- | Returns the number of ranges in the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.rangecount)
rangeCount :: Selection -> Effect Int
rangeCount = runEffectFn1 _rangeCount

foreign import _type :: EffectFn4 SelectionType SelectionType SelectionType Selection SelectionType

-- | Returns a string describing the type of the current selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.type)
type_ :: Selection -> Effect SelectionType
type_ = runEffectFn4 _type SelectionType.None SelectionType.Caret SelectionType.Range

foreign import _addRange :: EffectFn2 Range Selection Unit

-- | A `Range` object that will be added to the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.addrange)
addRange :: Range -> Selection -> Effect Unit
addRange = runEffectFn2 _addRange

foreign import _collapse :: EffectFn3 Node (Maybe Int) Selection Unit

-- | Collapses the current selection to a single point.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.collapse)
collapse :: Node -> Maybe Int -> Selection -> Effect Unit
collapse = runEffectFn3 _collapse

foreign import _collapseToEnd :: EffectFn1 Selection Unit

-- | Collapses the selection to the end of the last range in the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.collapsetoend)
collapseToEnd :: Selection -> Effect Unit
collapseToEnd = runEffectFn1 _collapseToEnd

foreign import _collapseToStart :: EffectFn1 Selection Unit

-- | Collapses the selection to the start of the first range in the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.collapsetostart)
collapseToStart :: Selection -> Effect Unit
collapseToStart = runEffectFn1 _collapseToStart

foreign import _containsNode :: EffectFn3 Node Boolean Selection Boolean

-- | Indicates if a certain node is part of the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.containsnode)

containsNode :: Node -> Boolean -> Selection -> Effect Boolean
containsNode = runEffectFn3 _containsNode

foreign import _deleteFromDocument :: EffectFn1 Selection Unit

-- | Deletes the selection's content from the document.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.deletefromdocument)
deleteFromDocument :: Selection -> Effect Unit
deleteFromDocument = runEffectFn1 _deleteFromDocument

foreign import _extend :: EffectFn3 Node Int Selection Unit

-- | Moves the focus of the selection to a specified point.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.extend)

extend :: Node -> Int -> Selection -> Effect Unit
extend = runEffectFn3 _extend

foreign import _getRangeAt :: EffectFn2 Int Selection Unit

-- | Returns a `Range` object representing one of the ranges currently selected.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.getrangeat)

getRangeAt :: Int -> Selection -> Effect Unit
getRangeAt = runEffectFn2 _getRangeAt

foreign import _modify :: EffectFn4 String String String Selection Unit

-- | Changes the current selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.modify)

modify :: String -> String -> String -> Selection -> Effect Unit
modify = runEffectFn4 _modify

foreign import _removeRange :: EffectFn2 Range Selection Unit

-- | Removes a range from the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.removerange)

removeRange :: Range -> Selection -> Effect Unit
removeRange = runEffectFn2 _removeRange

foreign import _removeAllRanges :: EffectFn1 Selection Unit

-- | Removes all ranges from the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.removeallranges)

removeAllRanges :: Selection -> Effect Unit
removeAllRanges = runEffectFn1 _removeAllRanges

foreign import _selectAllChildren :: EffectFn2 Node Selection Unit

-- | Adds all the children of the specified node to the selection.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.selectallchildren)

selectAllChildren :: Node -> Selection -> Effect Unit
selectAllChildren = runEffectFn2 _selectAllChildren

foreign import _setBaseAndExtent :: EffectFn5 Node Int Node Int Selection Unit

-- | Sets the selection to be a range including all or parts of two specified DOM nodes, and any content located between them.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.setbaseandextent)

setBaseAndExtent :: Node -> Int -> Node -> Int -> Selection -> Effect Unit
setBaseAndExtent = runEffectFn5 _setBaseAndExtent

foreign import _toString :: EffectFn1 Selection String

-- | Returns a string currently being represented by the selection object, i.e. the currently selected text.
-- |
-- | Source: [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Selection#selection.tostring)

toString :: Selection -> Effect String
toString = runEffectFn1 _toString
